//Description � Module that creates the Son.exe process with the relevent argument 

#define _CRT_SECURE_NO_WARNINGS 
#define TIMEOUT_IN_MILLISECONDS 10000
#define BRUTAL_TERMINATION_CODE 0x55

#include "create_process.h"

//  : calling CreateProcessSimple to create Son.exe process + sending the simple equation as an argument ,
// handling the process creation,  and getting the result and sending it back to Break_Line_To_Parts 
// Input : command line - "Son.exe + short_string(simple equation)" (for example: "Son.exe 1+1")
// Returns : result from Son.exe
PROCESS_INFORMATION CreateProcess_keep_open(char argument[])
{
	PROCESS_INFORMATION procinfo;
	BOOL				retVal;
	CHAR				command[MAX_COMMAND_LINE]; /* <ISP> TCHAR is a win32  */
													/* generic char which may be either a simple (ANSI) char or a unicode char, */
													/* depending on behind-the-scenes operating system definitions. Type LPTSTR */
													/* is a string of TCHARs. Type LPCTSTR is a const string of TCHARs. */

													/*  Start the child process. */
	
	strcpy(command, argument);


	retVal = CreateProcessSimple(command, &procinfo);


	if (retVal == 0)
	{
		printf("Process Creation Failed! %s\n", command);
		
	}
	return procinfo;

}

// CloseProcess_get_info : return exit_code, close handles for single process
// Input : procinfo
// Returns : exit_code of process
int CloseProcess_get_info(PROCESS_INFORMATION procinfo)
{
	DWORD exitcode;
	// If the function succeeds, the return value is nonzero.
	if (GetExitCodeProcess(procinfo.hProcess, &exitcode) == 0)
	{
		printf("Process GetExitCode Failed!\n");
		exitcode = RET_FAIL_NOVALUE;
	}

	
	CloseHandle(procinfo.hProcess); /* Closing the handle to the process */
	CloseHandle(procinfo.hThread); /* Closing the handle to the main thread of the process */
	return exitcode;
}

// wait_for_multiple_open_Process : wait for all processes. terminate all if fail
// Input : process_handles, process_num
// Returns : RET_SUCCESS / RET_FAIL
int wait_for_multiple_open_Process(HANDLE process_handles[], int process_num)
{
	DWORD WaitRes;
	int i = 0;
	WaitRes = WaitForMultipleObjects(
		process_num,
		process_handles,
		TRUE /* wait for all */,
		TIMEOUT_IN_MILLISECONDS);

	// WAIT_OBJECT_0 indicates that the state of all specified objects is signaled.
	if (WaitRes != WAIT_OBJECT_0)
	{
		printf("Waiting for processes failed. Ending program.\n");
		for (i = 0; i < process_num; i++)
		{
			TerminateProcess(process_handles[i], BRUTAL_TERMINATION_CODE);
			
		}
		Sleep(10);
		return RET_FAIL;
	}
	return RET_SUCCESS;
}



// CreateProcessSimpleMain : calling CreateProcessSimple, wait, get exit code, and close the handles
// Input : command line
// Returns : exit_code of process
int CreateProcessSimpleMain(char line[])
{
	PROCESS_INFORMATION procinfo;
	DWORD				waitcode;
	DWORD				exitcode;
	BOOL				retVal;
	CHAR				command[MAX_COMMAND_LINE] ; /* <ISP> TCHAR is a win32  */
													/* generic char which may be either a simple (ANSI) char or a unicode char, */
													/* depending on behind-the-scenes operating system definitions. Type LPTSTR */
													/* is a string of TCHARs. Type LPCTSTR is a const string of TCHARs. */

													/*  Start the child process. */

	strcpy(command, line);
	retVal = CreateProcessSimple(command, &procinfo);


	if (retVal == 0)
	{
		printf("Process Creation Failed!\n");
		return;
	}

	
	waitcode = WaitForSingleObject(
		procinfo.hProcess,
		TIMEOUT_IN_MILLISECONDS); // Waiting 5 secs for the process to end 
	/*
	printf("WaitForSingleObject output: ");
	switch (waitcode)
	{
	case WAIT_TIMEOUT:
		printf("WAIT_TIMEOUT\n"); break;
	case WAIT_OBJECT_0:
		printf("WAIT_OBJECT_0\n"); break;
	default:
		printf("0x%x\n", waitcode);
	}
	*/

	if (waitcode == WAIT_TIMEOUT) // Process is still alive 
	{
		printf("Process was not terminated before timeout!\n"
			"Terminating brutally!\n");
		TerminateProcess(
			procinfo.hProcess,
			BRUTAL_TERMINATION_CODE); //Terminating process with an exit code of 55h 
		Sleep(10); // Waiting a few milliseconds for the process to terminate 
	}
	

	GetExitCodeProcess(procinfo.hProcess, &exitcode);

	CloseHandle(procinfo.hProcess); /* Closing the handle to the process */
	CloseHandle(procinfo.hThread); /* Closing the handle to the main thread of the process */
	return exitcode;
}


// CreateProcessSimple : Help function , calling CreateProcess
// Input : command line  , process info
// Returns : retVal of the process
BOOL CreateProcessSimple(LPTSTR CommandLine, PROCESS_INFORMATION *ProcessInfoPtr)
{
	STARTUPINFO	startinfo = { sizeof(STARTUPINFO), NULL, 0 }; /* <ISP> here we */
															  /* initialize a "Neutral" STARTUPINFO variable. Supplying this to */
															  /* CreateProcess() means we have no special interest in this parameter. */
															  /* This is equivalent to what we are doing by supplying NULL to most other */
															  /* parameters of CreateProcess(). */

	return CreateProcess(NULL, /*  No module name (use command line). */
		CommandLine,			/*  Command line. */
		NULL,					/*  Process handle not inheritable. */
		NULL,					/*  Thread handle not inheritable. */
		FALSE,					/*  Set handle inheritance to FALSE. */
		NORMAL_PRIORITY_CLASS,	/*  creation/priority flags. */
		NULL,					/*  Use parent's environment block. */
		NULL,					/*  Use parent's starting directory. */
		&startinfo,				/*  Pointer to STARTUPINFO structure. */
		ProcessInfoPtr			/*  Pointer to PROCESS_INFORMATION structure. */
	);
}