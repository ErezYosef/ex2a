#pragma once

#include <stdio.h>
#include <windows.h>
#include "HardCodedData.h"

int CreateProcessSimpleMain(char line[]);

BOOL CreateProcessSimple(LPTSTR CommandLine, PROCESS_INFORMATION *ProcessInfoPtr);

PROCESS_INFORMATION CreateProcess_keep_open(char argument[]);

int CloseProcess_get_info(PROCESS_INFORMATION procinfo);

int wait_for_multiple_open_Process(HANDLE process_handles[], int process_num);

