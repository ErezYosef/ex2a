#pragma once

#include <stdio.h>
#include <string.h> 

#include <stdlib.h>
#include "HardCodedData.h"
#include "create_process.h"
#include "general.h"

//#include <ctype.h>

int manage_flow(char file_path[]);
int manage_close_processes(int ids_count, PROCESS_INFORMATION process_info[], DWORD process_exit_code[]);
int manage_open_processes(int ids_count, HANDLE process_handles[],
	PROCESS_INFORMATION process_info[], char file_path[], int ids[]);