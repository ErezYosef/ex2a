#define _CRT_SECURE_NO_WARNINGS 


#include "general.h"



// Read_array_file:	Read the int array from file to array
//input arguments :	Read file name, the nums array
// result argument:	The amount of lines that read
int Read_array_file(int num_array[], int len_array, char file_name[])
{
	FILE* file_pointer = fopen(file_name, "r");
	if (file_pointer == NULL)
	{
		printf("Error: opening %s failed\n", file_name);
		return RET_FAIL_NOVALUE;
	}
	int arr_position = 0, value = 0;
	char cur_line[MAX_ID_LEN];
	
	for (arr_position = 0; arr_position < len_array; arr_position++)
	{
		num_array[arr_position] = 0;
	}
	arr_position = 0;

	while (fgets(cur_line, MAX_ID_LEN, file_pointer) != NULL)
	{
		value = (int)strtol(cur_line, NULL, 10);
		num_array[arr_position] = value;
		arr_position++;
		if (arr_position == len_array)
			break;
	}

	fclose(file_pointer);
	return arr_position;


}

// Read_paths_num_files : reads single number from txt files in different and changing paths
// Input : ids, nums, len_ids, base_file_name
// Returns : RET_SUCCESS / RET_FAIL
int Read_paths_num_files(int ids[], int nums[], int len_ids, char base_file_name[])
{
	int i = 0;
	//char file_path[MAX_COMMAND_LINE];
	char std_ids_fname[MAX_COMMAND_LINE];
	char id_to_str[MAX_ID_LEN];
	//strcpy(file_path, argv[1]);

	for (i = 0; i < len_ids; i++)
	{
		strcpy(std_ids_fname, base_file_name);
		strcat(std_ids_fname, "/grades_");
		sprintf(id_to_str, "%d", ids[i]);
		strcat(std_ids_fname, id_to_str);
		strcat(std_ids_fname, "/final_");
		strcat(std_ids_fname, id_to_str);
		strcat(std_ids_fname, ".txt");
		// we get: 'base_file_name'/grades_123456789/final_123456789.txt
		if (Read_number_file( nums+i , std_ids_fname) == RET_FAIL)
		{
			printf("Cant read the grade file %s. exit..", std_ids_fname);
			return RET_FAIL;
		}
	}
	return RET_SUCCESS;

}

// Read_number_file : read single number from file_name and put in *num_p
// Input : *num_p , file_name
// Returns : 0 for cuccess, 1 for fail
int Read_number_file(int *num_p, char file_name[])
{
	return !Read_array_file(num_p, 1, file_name);
	// return 0 if success
}


/*
int append_new_string_to_file(char line[])
{
	FILE* file_pointer = fopen("Computation.txt", "a");
	if (file_pointer == NULL)
	{
		printf("FATAL ERROR- Couldn't open file- Exiting...");
		return -1;
	}
	fprintf(file_pointer, "%s\n", line);
	fclose(file_pointer);
	return 0;
}
*/

// Write_arr_to_file:	write arrays as needed to fname
//input arguments :	fname, arr1, arr2, arr_len
// result argument:	RET_SUCCESS / RET_FAIL
int Write_arr_to_file(char fname[], int arr1[], int arr2[], int arr_len)
{
	int i = 0;
	FILE *file_pointer = NULL;
	file_pointer = fopen(fname, "w");
	if (file_pointer == NULL)
	{
		printf("fatal error couldn't open file\n"); //Error opening file exit with code 1
		return RET_FAIL;
	}
	for (i = 0; i < arr_len; i++)
	{
		fprintf(file_pointer, "%d %d\n", arr1[i], arr2[i]);
	}
	

	fclose(file_pointer);
	return RET_SUCCESS;
}



