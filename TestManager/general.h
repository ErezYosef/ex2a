#pragma once
#include <stdio.h>
#include "HardCodedData.h"

int Read_array_file(int num_array[], int len_array, char file_name[]);
int Read_paths_num_files(int ids[], int nums[], int len_ids, char base_file_name[]);


int Read_number_file(int *num_p, char file_name[]);
int Write_arr_to_file(char fname[], int arr1[], int arr2[], int arr_len);