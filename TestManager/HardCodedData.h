#pragma once
#define MAX_IDS 10
#define MAX_ID_LEN 10
#define MAX_COMMAND_LINE 300

#define RET_SUCCESS 0
#define RET_FAIL 1
#define RET_FAIL_NOVALUE -1