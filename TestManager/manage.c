#define _CRT_SECURE_NO_WARNINGS 

#include "manage.h"

// manage_flow : get the path to files and manage the flow of the program
// Input : files path
// Returns : RET_SUCCESS / RET_FAIL
int manage_flow(char file_path[])
{
	int i = 0, ids_count = 0, opened_processes = 0;
	int err_indicator = 0, err_exitcode = 0;
	int ids[MAX_IDS] = { 0 };
	int read_grades[MAX_IDS] = { 0 };
	char std_ids_fname[MAX_COMMAND_LINE];
	char write_fname[MAX_COMMAND_LINE];

	HANDLE process_handles[MAX_IDS];
	PROCESS_INFORMATION process_info[MAX_IDS];
	DWORD process_exit_code[MAX_IDS];

	

	// Read the IDs to ids
	// get the ids count
	strcpy(std_ids_fname, file_path);
	strcat(std_ids_fname, "/student_ids.txt");
	ids_count = Read_array_file(ids, MAX_IDS, std_ids_fname);
	if (ids_count == RET_FAIL_NOVALUE)
		return RET_FAIL;

	// Open the processes
	opened_processes = manage_open_processes(ids_count, process_handles,
		process_info, file_path, ids);

	if (opened_processes != ids_count)
	{// ERROR in open processes - not all processes opened
		printf("Processes Creation faild on process %d of total %d\n", opened_processes, ids_count);
		err_indicator++;
		ids_count = opened_processes;
	}
	
	// Wait to the finish of the processes computation
	if (wait_for_multiple_open_Process(process_handles, ids_count) == RET_FAIL)
	{
		err_indicator++;		
	}
	// Collect exit codes, Close processes Handles
	err_exitcode = manage_close_processes(ids_count, process_info, process_exit_code);
	
	if ( (err_exitcode == RET_FAIL) && (err_indicator ==0) )
	{
		printf("TestManager: Error on GetExitCodes!\n");
		err_indicator++;
	}
	// if ANY error accure - stop and exit
	if  (err_indicator != 0) 
	{
		return RET_FAIL;
	}

	printf("The grades have arrived, captain");

	//if (Read_paths_num_files(ids, read_grades, ids_count, file_path) == RET_FAIL)
		//return RET_FAIL;


	strcpy(write_fname, file_path);
	strcat(write_fname, "/final_grades.txt");
	return Write_arr_to_file(write_fname, ids, process_exit_code, ids_count);

}

// manage_close_processes : close the handles of the processes and get the exit code to array
// Input : ids_count, process_info, process_exit_code
// Returns : RET_SUCCESS / RET_FAIL
int manage_close_processes(int ids_count, PROCESS_INFORMATION process_info[], DWORD process_exit_code[])
{
	int i = 0;
	int err_exitcode = RET_SUCCESS;
	for (i = 0; i < ids_count; i++)
	{

		process_exit_code[i] = CloseProcess_get_info(process_info[i]);
		if (process_exit_code[i] == -1)
			err_exitcode = RET_FAIL;

	}
	return err_exitcode;
}

// manage_open_processes : create the processes and put their info in the arrays
// Input : ids_count, process_handles, process_info, file_path, ids
// Returns : The amount of processes that opened
int manage_open_processes(int ids_count, HANDLE process_handles[],
	PROCESS_INFORMATION process_info[], char file_path[], int ids[])
{
	int i = 0;
	char process_command_base[MAX_COMMAND_LINE] = "TestGrade.exe ";
	char process_command[MAX_COMMAND_LINE];
	char id_to_str[MAX_ID_LEN];
	for (i = 0; i < ids_count; i++)
	{
		strcpy(process_command, process_command_base);
		strcat(process_command, file_path);
		strcat(process_command, "/grades_");
		sprintf(id_to_str, "%d", ids[i]);
		strcat(process_command, id_to_str);
		process_info[i] = CreateProcess_keep_open(process_command);
		if (process_info[i].hProcess == 0)
			return i;
		process_handles[i] = process_info[i].hProcess;

	}
	return i;
}