#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <string.h> 
#include <ctype.h>
#include <stdlib.h>
#include "HardCodedData.h"
#include "create_process.h"
#include "general.h"

// main : getting the path to files and write output file
// Input : int argc, char *argv[]
// Returns : 0 when all process made properly and no error accord -- RET_SUCCESS / RET_FAIL
int main(int argc, char* argv[])
{
	int status = 0;
	status = manage_flow(argv[1]);

	if (status == RET_FAIL)
		return RET_FAIL;

	printf("Succesful end!!");
	return RET_SUCCESS;
	
}